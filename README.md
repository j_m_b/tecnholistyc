# TecnHolistyc

Tecnolistic is a blog of technological discussion type social network created with Django, where users can create posts, comments and groups.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes. 

### Prerequisites

You must have installed on your local machine:

* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [Docker](https://docs.docker.com/get-docker/)
* [Docker Compose](https://docs.docker.com/compose/install/)

### Installing

An advantage of docker is that with few commands you can build, configure and set up a project. Follow this steps to set up this project.


Download a copy of the project:

```
git clone https://gitlab.com/j_m_b/tecnholistyc.git
```

Go into the project directory

```
cd tecnholistyc
```

Initialize the project (this can take a while)

```
sudo docker-compose up --build -d
```

After you see this console message it means you are near to finnish the instalation

```
Creating tecnholistyc_db_tecnholistyc_1 ... done
Creating tecnholistyc_tecnholistycapp_1 ... done
Creating tecnholistyc_nginx_1 ... done
```

Now you must to do the migration to the database with

```
sudo docker-compose exec tecnholistycapp python manage.py makemigrations
sudo docker-compose exec tecnholistycapp python manage.py migrate

```

And thats all, you can access to this project by going to your [localhost](http://localhost/).

## Author

* **Jonathan Essau MB** - *Computer engineer* - [essau.co](https://essau.co/)
