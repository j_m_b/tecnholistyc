from django.shortcuts import render, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse_lazy, reverse

from django.contrib import messages

from Groups.models import Group, Membership
from Groups.forms import GroupForm

from django.contrib.auth.models import User
from django.contrib.auth import get_user_model

from django.views.generic import ListView, DetailView, DeleteView
# Bootstrap modal views
from bootstrap_modal_forms.generic import (BSModalCreateView,
                                            BSModalUpdateView,
                                            BSModalReadView,
                                            BSModalDeleteView)


from django.utils.text import slugify




# Create your views here.


class ListGroupsView(LoginRequiredMixin, ListView):
    login_url = '/login/'
    redirect_field_name = 'Groups/group_list.html'
    model = Group
    template_name = 'Groups/group_list.html'
    paginate_by = 100

    def get_queryset(self):
        query = self.request.GET.get('group')

        if query:
            object_list = Group.objects.filter(name__icontains=query).order_by('name')
        else:
            object_list = Group.objects.order_by('name')

        return object_list


class PostListView(LoginRequiredMixin, DetailView):
    login_url = '/login/'
    redirect_field_name = 'Groups/group_list.html'
    template_name = 'Groups/posts_list.html'
    model = Group


class UserListGroupsView(LoginRequiredMixin, DetailView):
    '''
    THIS IS A DETAIL VIEW THAT SENDS A LIST OF A USER
    '''
    login_url = '/login/'
    redirect_field_name = 'Groups/user_group_list.html'
    model = User
    template_name = 'Groups/user_group_list.html'
    paginate_by = 100



    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        group_list = Membership.objects.select_related('user').all()
        context['group_list'] = group_list


        return context

class DetailGroupView(LoginRequiredMixin, DetailView):
    login_url = '/login/'
    redirect_field_name = 'Groups/group_detail.html'
    model = Group
    template_name = 'Groups/group_detail.html'

class GroupCreateView(LoginRequiredMixin, BSModalCreateView):
    login_url = '/login/'
    redirect_field_name = 'Groups/group_list.html'
    model = Group
    template_name = 'Groups/modals/group_create.html'
    form_class = GroupForm
    success_message = 'Grupo creado exitosamente'
    slug = lambda self: self.request.POST.get('name', None)+'-de-'+self.request.user.username


    def form_valid(self, form):
        print(".............................")
        print(self.request.FILES)
        print(".............................")
        name = form.cleaned_data['name']
        user = self.request.user.username
        form.instance.user = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('Groups:user_list_group', kwargs={'pk':self.request.user.pk})



class GroupUpdateView(LoginRequiredMixin, BSModalUpdateView):
    login_url = '/login/'
    redirect_field_name = 'Groups/group_list.html'
    model = Group
    template_name = 'Groups/modals/group_create.html'
    form_class = GroupForm
    success_message = 'Grupo actializado exitosamente'
    slug = ''

    def form_valid(self, form):
        self.slug = form.cleaned_data['name']

        if self.request.FILES :
            print(".............................")
            print(self.request.FILES)
            print(".............................")

        return super().form_valid(form)





class GroupDeleteView(LoginRequiredMixin, DeleteView):
    login_url = '/login/'
    redirect_field_name = 'Groups/group_list.html'
    model = Group
    success_url = reverse_lazy('Groups:index')

class GroupMembersView(LoginRequiredMixin, DetailView):
    login_url = '/login/'
    redirect_field_name = 'Groups/group_list.html'
    model = Group
    template_name = 'Groups/group_members.html'



@login_required
def join_group(request, pk):
    '''
    this method can alse unjoin the target group
    request.user.pk = user logged
    pk = target group
    '''
    user = get_object_or_404(User, pk=request.user.pk)
    group = get_object_or_404(Group, pk=pk)
    # check if exist
    register = Membership.objects.filter(user=user).filter(group=group)
    if not register:
        if group.group_type == '1':
            join = Membership(user=user, group=group)
        else:
            join = Membership(user=user, group=group, member_status=False)
        join.save()
    else: 
        register.delete()

    return HttpResponseRedirect(reverse('Groups:detail_group', kwargs={'slug': group.slug}))


@login_required
def membership_action(request, pk, slug, action):
    '''
    action approve = 1
    action remove = 2
    '''
    user = get_object_or_404(User, pk=pk)
    group = get_object_or_404(Group, slug=slug)
    membership = Membership.objects.filter(user=user).filter(group=group)

    if membership and group.user == request.user:
        if action == 2:
            membership.delete()
        elif action == 1:
            membership.update(member_status=True)

    return HttpResponseRedirect(reverse('Groups:members', kwargs={'slug': group.slug}))
    

@login_required
def invite_new_member(request, slug):
    '''
    Not user found code 0
    User inviting him self code 1
    user already on group code 2
    user found and added code 3
    user hacking group by url get method code 4
    '''

    username = request.GET.get('name', None)

    try:
        user = get_object_or_404(User, username=username)
    except:
        user = None
        

    group = get_object_or_404(Group, slug=slug)
    membership = Membership.objects.filter(user=user).filter(group=group)

    if request.user != group.user:
        return HttpResponseRedirect(reverse('Groups:members', kwargs={'slug': group.slug}))
    elif not user:
        data = {
            'status': 200,
            'message': 'Usuario no existe',
            'code': 0
        }
        return JsonResponse(data)
    elif user == request.user:
        data = {
            'status': 200,
            'message': 'Ya eres miembro del grupo',
            'code': 1
        }
        return JsonResponse(data)
    elif membership:
        data ={
            'status':200,
            'message': 'Usuario ya es miembro de grupo',
            'code': 2
        }
        return JsonResponse(data)
    elif not membership and user != request.user and group.user == request.user:
        Membership.objects.create(user=user, group=group)
        data = {
            'status':200,
            'message': 'Usuario agregado exitosamente',
            'code': 3
        }
        return JsonResponse(data)
