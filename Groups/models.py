from django.db import models

from django.dispatch import receiver
from django.db.models.signals import post_save
from django.urls import reverse_lazy, reverse


from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.conf import settings

from django.utils.text import slugify

from django.core.files.storage import FileSystemStorage
fs = FileSystemStorage(location='/media/group_images')
# Create your models here.


from django import template
register = template.Library()

class Group(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='created_groups')
    name = models.CharField(max_length=64)
    slug = models.SlugField(allow_unicode=True, unique=True)
    description = models.TextField(max_length=256, blank=True)
    image_reference = models.ImageField(storage=fs, blank=True)

    CATEGORIES = (
    ('0', 'Economia digital'),
    ('1', 'Video juegos'),
    ('2', 'Geek'),
    ('3', 'Arte'),
    ('4', 'Ciencia'),
    ('5', 'Educacion'),
    ('6', 'Ethical hacking'),
    ('7', 'Desarrollo de software'),
    ('8', 'Hardware & Electronica'),
    ('9', 'Inteligencia artifial'),
    ('10', 'Solo tecnologia'),
    )
    category = models.CharField(max_length=2, choices=CATEGORIES)
    
    GROUP_TYPES = (
        ('1', 'Publico'),
        ('2', 'Privado'),
    )
    group_type = models.CharField(max_length=1, choices=GROUP_TYPES)

    members = models.ManyToManyField(User, through='Membership', related_name='group_members')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name+'-de-'+self.user.username)
        return super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("Groups:detail_group", kwargs={"slug": self.slug})


    def __str__(self):
        return self.name


class Membership(models.Model):
    user = models.ForeignKey(User, default='', on_delete=models.CASCADE, related_name='group_list')
    group = models.ForeignKey(Group, default='', on_delete=models.CASCADE, related_name='member_list')
    member_status = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)




    def __str__(self):
        return '{} member of {}'.format(self.user, self.group)





# After user create gruop, this create a membership for the owner
@receiver(post_save, sender=Group)
def create_membership(sender, instance, created, **kwargs):
    if created:
        Membership.objects.create(user=instance.user, group=instance)
    






