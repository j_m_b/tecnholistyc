from django.urls import path

from Groups import views


app_name = 'Groups'

urlpatterns = [
    path('', views.ListGroupsView.as_view(), name='index'),
    path('<int:pk>/list', views.UserListGroupsView.as_view(), name='user_list_group'),
    path('create/', views.GroupCreateView.as_view(), name='create_group'),
    path('<slug:slug>/detail/', views.DetailGroupView.as_view(), name='detail_group'),
    path('<slug:slug>/update/', views.GroupUpdateView.as_view(), name='update_group'),
    path('<slug:slug>/delete/', views.GroupDeleteView.as_view(), name='delete_group'),
    path('join/<int:pk>', views.join_group, name='join'),
    path('<slug:slug>/members/', views.GroupMembersView.as_view(), name='members'),
    path('<slug:slug>/membership-action/<int:pk>/<int:action>', views.membership_action, name='membership_action'),
    path('<slug:slug>/invite-new-member/', views.invite_new_member, name='invite_new_member'),
    path('<slug:slug>/posts', views.PostListView.as_view(), name='posts_group'),
    path('search/',views.ListGroupsView.as_view(), name='search')

    
]