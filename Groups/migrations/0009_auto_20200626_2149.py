# Generated by Django 3.0.3 on 2020-06-26 21:49

import django.core.files.storage
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Groups', '0008_auto_20200306_0317'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='image_reference',
            field=models.ImageField(blank=True, storage=django.core.files.storage.FileSystemStorage(location='/media/group_images'), upload_to=''),
        ),
    ]
