# Generated by Django 3.0.3 on 2020-03-06 00:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Groups', '0006_membership_updated_at'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='category',
            field=models.CharField(choices=[('0', 'Economia digital'), ('1', 'Video juegos'), ('2', 'Geek'), ('3', 'Arte'), ('4', 'Ciencia'), ('5', 'Educacion'), ('6', 'Ethical hacking'), ('7', 'Desarrollo de software'), ('8', 'Hardware & Electronica'), ('9', 'Inteligencia artifial'), ('10', 'Genernico')], default='10', max_length=2),
        ),
    ]
