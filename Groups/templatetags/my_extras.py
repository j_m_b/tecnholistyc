from django import template
register = template.Library()

from django.shortcuts import get_object_or_404
from Groups.models import Group, Membership
from Posts.models import Post, Comment, LikePost
from Accounts.models import FollowList, MessageWall, CommentMessageWall

from django.contrib.auth.models import User

from datetime import datetime


@register.filter(name='check_for_member_status')
def check_for_member_status(value, arg):
    user = get_object_or_404(User, pk=arg)
    group = get_object_or_404(Group, slug=value)
    membership = Membership.objects.filter(user=user).filter(group=group)
    state = True if membership[0].member_status == True else False
    
    return state


@register.filter(name='approved_group_list')
def approved_group_list(value):
    user = get_object_or_404(User, pk=value)
    group_list = Membership.objects.filter(user=user).filter(member_status=True)
    
    return group_list


@register.filter(name='approved_members_list')
def approved_members_list(value):
    group = get_object_or_404(Group, slug=value)
    members_list = Membership.objects.filter(group=group).filter(member_status=True)
    
    return members_list


@register.filter(name='not_approved_members_list')
def not_approved_members_list(value):
    group = get_object_or_404(Group, slug=value)
    members_list = Membership.objects.filter(group=group).filter(member_status=False)
    
    return members_list


@register.filter(name='check_for_comment_modifications')
def check_for_comment_modifications(value):
    comment = get_object_or_404(Comment, pk=value)
    created_time = comment.created_at.strftime("%Y/%m/%d a las %H:%M:%S")
    updated_time = comment.updated_at.strftime("%Y/%m/%d a las %H:%M:%S")

    if created_time == updated_time:
        date = 'Creado el {}'.format(created_time)
    else:
        date = 'Editado el {}'.format(updated_time)

    return date

@register.filter(name='check_for_liked_post')
def check_for_liked_post(value, arg):
    user = get_object_or_404(User, pk=value)    
    post = get_object_or_404(Post, pk=arg)
    register = LikePost.objects.filter(user=user).filter(post=post)

    status = True if register else False

    return status


@register.filter(name='check_for_follow')
def check_for_follow(value, arg):
    # user is current logged user
    # person is not the logged user
    user = get_object_or_404(User, pk=value)
    person = get_object_or_404(User, pk=arg)
    register = FollowList.objects.filter(user=user, follows=person)

    following = True if register else False

    return following

@register.filter(name='check_for_personal_posts')
def check_for_personal_posts(value):
    user = get_object_or_404(User, pk=value)
    posts = Post.objects.filter(user=user).filter(group=None)

    return posts

@register.filter(name='check_for_wall_modifications')
def check_for_wall_modifications(value, arg):

    if arg == 1:
        wall_message = get_object_or_404(MessageWall, pk=value)
    elif arg == 2:
        wall_message = get_object_or_404(CommentMessageWall, pk=value) 
    
    created_time = wall_message.created_at.strftime("%Y/%m/%d a las %H:%M:%S")
    updated_time = wall_message.updated_at.strftime("%Y/%m/%d a las %H:%M:%S")

    if created_time == updated_time:
        date = 'Creado el {}'.format(created_time)
    else:
        date = 'Editado el {}'.format(updated_time)

    return date


