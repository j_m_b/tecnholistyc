from django import forms
from bootstrap_modal_forms.mixins import PopRequestMixin, CreateUpdateAjaxMixin
from bootstrap_modal_forms.forms import BSModalForm
from Groups.models import Group


class GroupForm(BSModalForm):

    class Meta:
        model = Group
        fields = ('name', 
                'description', 
                'image_reference', 
                'category', 
                'group_type')