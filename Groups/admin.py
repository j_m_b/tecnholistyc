from django.contrib import admin
from Groups.models import Group, Membership

class MembershipInline(admin.TabularInline):
    model = Group.members.through

# Register your models here.
admin.site.register(Group)
admin.site.register(Membership)



