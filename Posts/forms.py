from mediumeditor.widgets import MediumEditorTextarea

from django import forms
from Posts.models import Post, Comment
from bootstrap_modal_forms.forms import BSModalForm



class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ['title', 'category', 'text' ]

        # adding class's to forms elements
        widgets = {
            'title':forms.TextInput(attrs={'placeholder':'dame un titulo', 'class':'form-control'}),
            'category': forms.Select(attrs={'class':'form-control'}),
            'text': MediumEditorTextarea(attrs={'class':'post-input-text'})
        }
    # 'text':forms.Textarea(attrs={'class':'editable', 'placeholder':'Escribe el contenido de tu post aqui, puede utilizar herramientas de medium'})


class CommentForm(BSModalForm):

    class Meta():
        model = Comment
        fields = ('text',)