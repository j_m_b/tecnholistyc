from django.urls import path
from Posts import views

app_name = 'Posts'

urlpatterns = [
    path('', views.PostListView.as_view(), name='index'),
    path('new/', views.PostCreateView.as_view(), name='create_user_post'),
    path('new/<slug:slug>/', views.PostCreateView.as_view(), name='create_post'),
    path('<int:pk>/', views.PostDetailView.as_view(), name='detail_post'),
    path('<int:pk>/edit/', views.PostUpdateView.as_view(), name='update_post'),
    path('<int:pk>/delete/', views.PostDeleteView.as_view(), name='delete_post'),
    path('like/<int:pk>/', views.post_like, name='post_like'),
    path('<int:pk>/new-comment/', views.CommentCreateView.as_view(), name='create_comment'),
    path('edit-comment/<int:pk>/', views.CommentUpdateView.as_view(), name='update_comment'),
    path('remove-comment/<int:pk>/', views.CommentDeleteView.as_view(), name='delete_comment'),
    path('created-by-me/', views.PersonalPostListView.as_view(), name='personal_post_list'),
    path('created-by-follows/', views.FollowsPostListView.as_view(), name='follow_post_list'),
    path('liked-posts/', views.LikedPostListView.as_view(), name='liked_post_list'),
    path('groups/', views.GroupPostListView.as_view(), name='group_post_list'),
    path('search/',views.PostListView.as_view(), name='search'),

    
]