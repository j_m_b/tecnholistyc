from django import template
register = template.Library()

from django.shortcuts import get_object_or_404
from Posts.models import Post, Comment, LikePost
from django.contrib.auth.models import User



@register.filter(name='check_for_comment_modifications')
def check_for_comment_modifications(value, arg):
    comment = get_object_or_404(Comment, pk=value)
    
    if comment.creates_at == comment.updated_at:
        date = 'Creado el {}'.format(comment.created_at)
    else:
        date = 'Editado el {}'.format(comment.updated_at)

    return date


