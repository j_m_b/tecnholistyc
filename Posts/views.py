from django.shortcuts import render, get_object_or_404

from django.contrib.auth.models import User

from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse_lazy, reverse

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required

from Posts.forms import PostForm, CommentForm
from Posts.models import Post, Comment, LikePost
from Groups.models import Group, Membership
from Accounts.models import FollowList


from django.views.generic import (ListView, 
                                CreateView, 
                                DetailView, 
                                UpdateView,
                                DeleteView)

# Bootstrap modal views
from bootstrap_modal_forms.generic import (BSModalCreateView,
                                            BSModalUpdateView,
                                            BSModalReadView,
                                            BSModalDeleteView)

# Create your views here.


class PostListView(ListView):
    model = Post
    template_name = 'Posts/posts_list.html'

    # This view pages its only for user profile posts
    def get_queryset(self):
        query = self.request.GET.get('post')

        if query:
            object_list = Post.objects.filter(title__icontains=query).filter(group=None).order_by('-created_at')
        else:
            object_list = Post.objects.filter(group=None).order_by('-created_at')

        return object_list


class PersonalPostListView(LoginRequiredMixin, ListView):
    login_url = '/index/'
    template_name_field = 'Posts/post_list.html'
    model = Post
    template_name = 'Posts/posts_list.html'

    def get_queryset(self):
        return Post.objects.filter(user=self.request.user)

class FollowsPostListView(LoginRequiredMixin, ListView):
    '''
    This is the list of the people i follow
    '''
    login_url = '/index/'
    template_name_field = 'Posts/post_list.html'
    model = Post
    template_name = 'Posts/posts_list.html'

    def get_queryset(self):
        '''
        Queryset > in FollowList model: user(person logged) follows(another user)
        filter(group=None) have be null because it must be only personal and not group posts
        '''
        list_of_follows = []
        for user_followed in self.request.user.following_list.all():
            list_of_follows.append(user_followed.follows)

        return Post.objects.filter(group=None).filter(user__in=list_of_follows)


class LikedPostListView(LoginRequiredMixin, ListView):
    '''
    This is the list of the people i follow
    '''
    login_url = '/index/'
    template_name_field = 'Posts/post_list.html'
    model = Post
    template_name = 'Posts/posts_list.html'

    def get_queryset(self):
        list_of_likes = []
        for post_liked in self.request.user.user_post_likes.all():
            list_of_likes.append(post_liked.post.pk)

        return Post.objects.filter(pk__in=list_of_likes)

class GroupPostListView(LoginRequiredMixin, ListView):
    '''
    This is the list of the groups that im
    '''
    login_url = '/index/'
    template_name_field = 'Posts/post_list.html'
    model = Post
    template_name = 'Posts/posts_list.html'

    def get_queryset(self):
        user = get_object_or_404(User, pk=self.request.user.pk)
        approved_groups = Membership.objects.filter(user=user).filter(member_status=True)
        print(approved_groups.all())
        group_list = []
        for post_grup in approved_groups.all():
            group_list.append(post_grup.group)

        return Post.objects.filter(group__in=group_list)


class PostDetailView(DetailView):
    model = Post
    template_name = 'Posts/post_detail.html'


class PostCreateView(LoginRequiredMixin, CreateView):
    login_url = '/index/'
    template_name_field = 'Posts/post_list.html'
    model = Post
    template_name = ''
    form = PostForm
    fields = ('title', 'text', 'category')

    def get_context_data(self,**kwargs):
        context = super(PostCreateView, self).get_context_data(**kwargs)
        
        if 'slug' in self.kwargs:
            group = get_object_or_404(Group, slug=self.kwargs['slug'])
            context['group'] = group
            context["form"]= self.form
            self.template_name = 'Posts/create_group_post.html'
            return context
        else:
            self.template_name = 'Posts/create_user_post.html'
            context["form"]= self.form
            return context



    def form_valid(self, form):
        form.instance.user = self.request.user
        
        if 'group' in self.request.POST :
            group = get_object_or_404(Group, slug=self.request.POST['group'])
            form.instance.group = group

        return super().form_valid(form)


class PostUpdateView(LoginRequiredMixin, UpdateView):
    login_url = '/login/'
    template_name_field = 'Posts/post_detail.html'
    model = Post
    fields = ('title', 'text', 'category')
    template_name = 'Posts/create_user_post.html'


class PostDeleteView(LoginRequiredMixin, DeleteView):
    login_url = '/login/'
    template_name_field = 'Posts/post_form.html'
    model = Post
    success_url = reverse_lazy('Posts:index')


@login_required
def post_like(request, pk):
    user = get_object_or_404(User, pk=request.user.pk)
    post = get_object_or_404(Post, pk=pk)

    register = LikePost.objects.filter(user=user).filter(post=post)

    if not register:
        LikePost.objects.create(user=user, post=post)
        likes_register = LikePost.objects.filter(post=post).count()
        data = {'message': 'register created',
                'likes': likes_register,
                'status': 1
                }
    else: 
        register.delete()
        likes_register = LikePost.objects.filter(post=post).count()
        data = {'message': 'register deleted',
                'likes': likes_register,
                'status': 0
                }

    return JsonResponse(data)
    # return HttpResponseRedirect(reverse('Posts:detail_post', kwargs={'pk': pk}))


class CommentCreateView(LoginRequiredMixin, BSModalCreateView):
    login_url = '/login/'
    template_name_field = 'Posts/post_detail.html'
    model = Comment
    template_name = 'Posts/modals/comment_create.html'
    form_class = CommentForm
    success_message = 'Comentario creado exitosamente'

    def form_valid(self, form, **kwargs):
        form.instance.user = self.request.user
        post = get_object_or_404(Post, pk=self.kwargs['pk'])
        form.instance.post = post
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('Posts:detail_post', kwargs={'pk': self.kwargs['pk']})


class CommentUpdateView(LoginRequiredMixin, BSModalUpdateView):
    login_url = '/login/'
    template_name_field = 'Posts/post_detail.html'
    model = Comment
    template_name = 'Posts/modals/comment_create.html'
    form_class = CommentForm
    success_message = 'Comentario editado exitosamente'
    
    def get_success_url(self):
        current_comment_post = get_object_or_404(Comment, pk=self.kwargs['pk'])
        return reverse_lazy('Posts:detail_post', kwargs={'pk': current_comment_post.post.pk})


class CommentDeleteView(LoginRequiredMixin, BSModalDeleteView):
    login_url = '/login/'
    template_name_field = 'Posts/post_detail.html'
    model = Comment
    template_name = 'Posts/modals/comment_delete.html'
    success_message = 'Comentario eliminado exitosamente'
    
    def get_success_url(self):
        current_comment_post = get_object_or_404(Comment, pk=self.kwargs['pk'])
        return reverse_lazy('Posts:detail_post', kwargs={'pk': current_comment_post.post.pk})