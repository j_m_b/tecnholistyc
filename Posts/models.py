from django.db import models
from django.urls import reverse_lazy, reverse
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.conf import settings
from Groups.models import Group, Membership

import misaka




# Create your models here.
class Post(models.Model):
    '''
    If post has not group, that means the posts its out of any group and 
    its store in the user profile
    '''
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='posts' ,on_delete=models.CASCADE)
    group = models.ForeignKey(Group, related_name='posts', blank=True, null=True ,on_delete=models.CASCADE)
    likes = models.ManyToManyField(User, through='LikePost', related_name='likes')
    title = models.CharField(max_length=64)
    text = models.TextField()
    text_html = models.TextField(editable=False)  # Determ if with editable option can i edit

    CATEGORIES = (
    ('0', 'Economia digital'),
    ('1', 'Video juegos'),
    ('2', 'Geek'),
    ('3', 'Arte'),
    ('4', 'Ciencia'),
    ('5', 'Educacion'),
    ('6', 'Ethical hacking'),
    ('7', 'Desarrollo de software'),
    ('8', 'Hardware & Electronica'),
    ('9', 'Inteligencia artifial'),
    ('10', 'Solo tecnologia'),
    )
    category = models.CharField(max_length=2, choices=CATEGORIES)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        self.text_html = misaka.html(self.text)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("Posts:detail_post", kwargs={"pk": self.pk})

    def __str__(self):
        return self.title

    class Meta:
        ordering = ["-created_at"]
        unique_together = ["user", "text"]


class LikePost(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='user_post_likes' ,on_delete=models.CASCADE)
    post = models.ForeignKey(Post, related_name='post_likes', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} likes {}'.format(self.user, self.post.title)



class Comment(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='comments' ,on_delete=models.CASCADE)
    post = models.ForeignKey(Post, related_name='comments', on_delete=models.CASCADE)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return reverse("Posts:detail", kwargs={"pk": self.post.pk})

    def __str__(self):
        return '{} in {}, {}'.format(self.user ,self.post, self.text)

    class Meta:
        ordering = ["-created_at"]
        unique_together = ["post", "text"]



