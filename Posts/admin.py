from mediumeditor.admin import MediumEditorAdmin

from django.contrib import admin
from Posts.models import Post, Comment, LikePost
# Register your models here.

@admin.register(Post)
class MyModelAdmin(MediumEditorAdmin, admin.ModelAdmin):
    mediumeditor_fields = ['text']

# admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(LikePost)

