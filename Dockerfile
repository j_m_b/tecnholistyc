# Base Image
FROM python:3

# create and set working directory
RUN mkdir -p /opt/services/tecnholistycapp/src/
WORKDIR /opt/services/tecnholistycapp/src/

# Add current directory code to working directory
ADD . /opt/services/tecnholistycapp/src/

# set default environment variables
ENV PYTHONUNBUFFERED 1
ENV LANG C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive 

# set project environment variables
# grab these via Python's os.environ
# these are 100% optional here
ENV PORT=80

# Install system dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
        tzdata \
        python3-setuptools \
        python3-pip \
        python3-dev \
        python3-venv \
        git \
	nano \
        && \
        apt-get clean && \
        rm -rf /var/lib/apt/lists/*


# install environment dependencies
RUN pip3 install --upgrade pip 
RUN pip3 install pipenv
RUN pip3 install -r requirements.txt

# Install project dependencies
RUN pipenv install --skip-lock --dev
RUN rm /usr/local/lib/python3.8/site-packages/mediumeditor/widgets.py
RUN cp /opt/services/tecnholistycapp/src/config/copy_files/widgets.py /usr/local/lib/python3.8/site-packages/mediumeditor/ 
RUN python manage.py collectstatic


EXPOSE 80
CMD gunicorn TecnHolistyc.wsgi:application --bind 0.0.0.0:$PORT
