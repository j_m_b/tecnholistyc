from django.views.generic import TemplateView

from django.urls import reverse_lazy
from TecnHolistyc.forms import LoginForm, CustomUserCreationForm
from django.db.models import Count
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from Accounts.models import UserProfile
from Posts.models import Post
from Groups.models import Group

# MODAL UTILS
from bootstrap_modal_forms.generic import BSModalLoginView, BSModalCreateView

current_user = get_user_model()

class IndexPage(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        posts = Post.objects.annotate(num_likes=Count('likes')).order_by('-num_likes')[:5]
        groups = Group.objects.annotate(num_member=Count('members')).order_by('-num_member')[:5]
        # profiles = UserProfile.objects.annotate(num_follows=Count('follow_list')).order_by('-num_follows')[:5]
        users = User.objects.annotate(num_post=Count('posts')).order_by('-num_post')[:5]
        context['posts'] = posts
        context['groups'] = groups
        context['users'] = users
        
        return context


# class AboutPage(TemplateView):
#     template_name = 'about.html'


class LoginView(BSModalLoginView):
    template_name = 'login.html'
    authentication_form = LoginForm
    success_message = 'Bienvenido'
    success_url = reverse_lazy('index')


class SignUpView(BSModalCreateView):
    form_class = CustomUserCreationForm
    template_name = 'signup.html'
    success_message = 'Hola, bienvenido'
    success_url = reverse_lazy('index')

    # def form_valid(self, form):

    #     user_form = CustomUserCreationForm(data=self.request.POST)
    #     user = user_form.save()

    #     new_profile = UserProfile.objects.create(user=user)
    #     new_profile.save()
    #     return super().form_valid(form)


