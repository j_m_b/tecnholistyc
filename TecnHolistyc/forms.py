from django.contrib.auth.models import  User
# from bootstrap_modal_forms.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from bootstrap_modal_forms.mixins import PopRequestMixin, CreateUpdateAjaxMixin



class LoginForm(AuthenticationForm):
    class Meta:
        model = User
        fields = ['username', 'password']


class CustomUserCreationForm(PopRequestMixin, CreateUpdateAjaxMixin,UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']