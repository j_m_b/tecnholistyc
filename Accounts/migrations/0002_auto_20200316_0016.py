# Generated by Django 3.0.3 on 2020-03-16 00:16

import django.core.files.storage
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Accounts', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='profile_pic',
            field=models.ImageField(blank=True, default='', storage=django.core.files.storage.FileSystemStorage(location='/media/profile_pics'), upload_to='profile_pics'),
        ),
    ]
