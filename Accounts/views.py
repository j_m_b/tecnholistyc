from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy, reverse
from django.http import HttpResponseRedirect, JsonResponse
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from Accounts.forms import UserProfileForm, UserContactLinkForm, MessageWallForm, CommentMessageWallForm
from Accounts.models import UserProfile, UserContactLink, FollowList, MessageWall, CommentMessageWall
from django.views.generic import (ListView, DetailView, 
                                    CreateView, UpdateView, 
                                    DeleteView)

# BOOTSTRAP MODAL
from bootstrap_modal_forms.generic import (BSModalLoginView,
                                            BSModalCreateView,
                                            BSModalUpdateView,
                                            BSModalReadView,
                                            BSModalDeleteView)



# Create your views here.

class ListUserView(LoginRequiredMixin, ListView):
    '''
    List all users
    '''
    login_url = '/login/'
    redirect_field_name = 'Accounts/user_list.html'
    model = User
    paginate_by = 100
    template_name = 'Accounts/user_list.html'


    def get_queryset(self):
        query = self.request.GET.get('username')

        if query:
            object_list = User.objects.filter(username__icontains=query).order_by('username')
        else:
            object_list = User.objects.order_by('username')

        return object_list

class DetailUserProfileView(LoginRequiredMixin,DetailView):
    '''
    THIS IS CURRENT USER
    '''
    login_url = '/login/'
    redirect_field_name = 'Accounts/user_list.html'
    model = get_user_model()
    template_name = 'Accounts/user_profile.html'


class PersonPostListView(LoginRequiredMixin, DetailView):
    login_url = '/login/'
    redirect_field_name = 'Accounts/person_posts.html'
    model = User
    template_name = 'Accounts/person_posts.html'


class PersonGroupListView(LoginRequiredMixin, DetailView):
    login_url = '/login/'
    redirect_field_name = 'Accounts/person_groups.html'
    model = User
    template_name = 'Accounts/person_groups.html'


class DetailPersonProfileView(LoginRequiredMixin,DetailView):
    '''
    THIS IS ANOTHER PERSON != CURRENT USER
    '''
    login_url = '/login/'
    redirect_field_name = 'Accounts/user_list.html'
    model = User
    template_name = 'Accounts/person_profile.html'


class UpdateProfileView(LoginRequiredMixin, BSModalUpdateView):
    login_url = '/login/'
    redirect_field_name = 'Accounts/user_profile.html'
    model = UserProfile
    template_name = 'modals/update_user_profile.html'
    form_class = UserProfileForm
    success_message = 'Perfil actualizado'
    success_url = reverse_lazy('index')
    
    def get_success_url(self):
        return reverse_lazy("Accounts:user_profile", kwargs={"pk": self.request.user.pk})

    def form_valid(self, form):
        print(".............................")
        print(self.request.FILES)
        print(".............................")

        return super().form_valid(form)


class MessageWallCreateView(LoginRequiredMixin, CreateView):
    login_url = '/index/'
    template_name_field = 'Accounts/person_profile.html'
    model = MessageWall
    template_name = 'Accounts/person_profile.html'
    form = MessageWallForm
    fields = ('text',)

    def form_valid(self, form):
        user_wall = get_object_or_404(User, pk=self.kwargs['pk'])
        form.instance.user_wall = user_wall
        form.instance.user_writer = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('Accounts:detail_profile', kwargs={'pk':self.kwargs['pk']})


class CommentMessageCreateView(LoginRequiredMixin, CreateView):
    login_url = '/index/'
    template_name_field = 'Accounts/person_profile.html'
    model = CommentMessageWall
    template_name = 'Accounts/person_profile.html'
    form = CommentMessageWallForm
    fields = ('text',)

    def form_valid(self, form):
        wall_message = get_object_or_404(MessageWall, pk=self.kwargs['pk'])
        form.instance.user_writer = self.request.user
        form.instance.wall_message = wall_message
        return super().form_valid(form)

    def get_success_url(self):
        wall_message = get_object_or_404(MessageWall, pk=self.kwargs['pk'])
        profile = wall_message.user_wall.pk
        return reverse_lazy('Accounts:detail_profile', kwargs={'pk':profile})


def get_wall_message(object_type, pk):
    if object_type == 1:    
        wall_message = get_object_or_404(MessageWall, pk=pk)
    elif object_type == 2:
        wall_message = get_object_or_404(CommentMessageWall, pk=pk)

    return wall_message


@login_required
def update_wall_message(request, object_type, pk):

    if request.method == 'POST':
        wall_message = get_wall_message(object_type,pk)

        if wall_message.user_writer == request.user:
            wall_message.text = request.POST.get('text', None)
            wall_message.save()
            data = {
                'status': 200,
                'message': 'Updated',
                'object_type': object_type,
                'text': wall_message.text
            }
            return JsonResponse(data)


@login_required
def delete_wall_message(request, object_type, pk):
    '''
    ONLY CAN DELETE WALL OWNER AND WALL MESSAGE CREATER
    --- > 
    '''
    wall_message = get_wall_message(object_type,pk)
    wall_message.delete()

    data = {
        'status': 200,
        'message': 'Deleted',
        'object_type': object_type
    }
    return JsonResponse(data)


@login_required
def create_follow_person(request, pk):
    '''
    this method can alse unfollow the target user
    request.user.pk = user logged
    pk = target user
    '''
    from_user = get_object_or_404(User, pk=request.user.pk)
    to_user = get_object_or_404(User, pk=pk)
    # check if exist
    register = FollowList.objects.filter(user=from_user).filter(follows=to_user)
    if not register:
        new_follow = FollowList(user=from_user, follows=to_user)
        new_follow.save()
        data = {
            'status': 200,
            'message': 'register created'
        }
    else: 
        register.delete()
        data = {
            'status': 200,
            'message': 'register deleted'
        }

    return JsonResponse(data)


class ListFollowsView(LoginRequiredMixin, ListView):
    login_url = '/login/'
    redirect_field_name = 'Accounts/follows_detail.html'
    model = User
    template_name = 'Accounts/follows_detail.html'


class ListContactUserLinkView(DetailView, LoginRequiredMixin):
    '''
    This is a detail view, not a list view... to charge to all user resources
    '''
    login_url = '/login/'
    redirect_field_name = 'Accounts/user_contact_link.html'
    model = get_user_model()
    template_name = 'Accounts/user_contact_link.html'


class CreateContactLinkView(CreateView, LoginRequiredMixin):
    login_url = '/login/'
    redirect_field_name = 'Accounts/user_contact_link.html'
    model = UserContactLink
    fields = ['url',]
    template_name = 'Accounts/user_contact_link.html'
    success_message = 'Link agregado exitosamente'

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("Accounts:list_user_links", kwargs={"pk": self.request.user.pk})


@login_required
def update_contact_link(request, pk):
    link = get_object_or_404(UserContactLink, pk=pk)

    if request.method == 'POST' and request.user == link.user:
        new_link = request.POST.get('url', None)
        link.url = new_link
        link.save()
        data = {
            'status': 200,
            'message': 'register updated',
            'link': new_link
        }

        return JsonResponse(data)


@login_required
def delete_contact_link(request, pk):
    link = get_object_or_404(UserContactLink, pk=pk)
    if request.method == 'POST' and request.user == link.user:
        link.delete()
        data = {
            'status': 200,
            'message': 'register deleted',
        }

        return JsonResponse(data)

