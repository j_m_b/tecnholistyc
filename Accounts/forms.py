from django import forms
from bootstrap_modal_forms.forms import BSModalForm

from Accounts.models import UserProfile, UserContactLink, MessageWall, CommentMessageWall


class UserProfileForm(BSModalForm):

    class Meta():
        model = UserProfile
        fields = ('about_me','profile_pic', 'portfolio_site')


class UserContactLinkForm(forms.Form):
    # THIS FORM MUST BE A BOOTSTRAP MODAL
    class Meta():
        model = UserContactLink
        fields = ('url',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["url"].label = "Red social de contacto"


class MessageWallForm(forms.ModelForm):

    class Meta():
        model = MessageWall
        fields = ('text',)



        widgets = {
            'text':forms.Textarea(attrs={'class':'form-control','rows':'3', 'placeholder':'Deja un mensaje en este muro'}),
        }



class CommentMessageWallForm(forms.ModelForm):

    class Meta():
        model = CommentMessageWall
        fields = ('text',)


