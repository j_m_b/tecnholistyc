from django.urls import path, re_path
from Accounts import views

from django.contrib.auth import get_user_model

app_name = 'Accounts'

urlpatterns = [
    path('<int:pk>/', views.DetailUserProfileView.as_view(), name='user_profile'),
    path('update/<int:pk>/', views.UpdateProfileView.as_view(), name='update_profile'),
    path('list/', views.ListUserView.as_view(), name='list_profiles'),
    path('profile/<int:pk>/', views.DetailPersonProfileView.as_view(), name='detail_profile'),
    path('profile/<int:pk>/post-list/', views.PersonPostListView.as_view(), name='person_post_list'),
    path('profile/<int:pk>/group-list/', views.PersonGroupListView.as_view(), name='person_group_list'),
    path('follow/<int:pk>/', views.create_follow_person, name='follow'),
    path('follow/list/', views.ListFollowsView.as_view(), name='follow_list'),
    path('new-wall-message/<int:pk>', views.MessageWallCreateView.as_view(), name='create_message_wall'),
    path('new-comment-message/<int:pk>', views.CommentMessageCreateView.as_view(), name='create_message_comment'),
    path('update-wall-message/<int:object_type>/<int:pk>/', views.update_wall_message, name='update_wall_message'),
    path('delete-wall-message/<int:object_type>/<int:pk>/', views.delete_wall_message, name='delete_wall_message'),
    path('search/', views.ListUserView.as_view(), name='search'),
    path('<int:pk>/links/', views.ListContactUserLinkView.as_view(), name='list_user_links'),
    path('new-user-link/', views.CreateContactLinkView.as_view(), name='new_user_link'),
    path('update-user-link/<int:pk>/', views.update_contact_link, name='update_user_link'),
    path('delete-user-link/<int:pk>/', views.delete_contact_link, name='delete_user_link'),
    
]