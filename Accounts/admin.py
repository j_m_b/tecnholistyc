from django.contrib import admin
from Accounts.models import UserProfile, UserContactLink, FollowList, MessageWall, CommentMessageWall

# Register your models here.

class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'Profile'
    fk_name = 'user'

admin.site.register(UserProfile)
admin.site.register(UserContactLink)
admin.site.register(FollowList)
admin.site.register(MessageWall)
admin.site.register(CommentMessageWall)


