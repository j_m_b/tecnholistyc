from django.utils import timezone
from django.urls import reverse
from django.conf import settings
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.db import models
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
fs = FileSystemStorage(location='/media/profile_pics')

# Signal reciver
# https://docs.djangoproject.com/es/3.0/topics/signals/
# https://simpleisbetterthancomplex.com/tutorial/2016/11/23/how-to-add-user-profile-to-django-admin.html
from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.

class FollowList(models.Model):
    user = models.ForeignKey(get_user_model(), default='', related_name='following_list',on_delete=models.CASCADE)
    follows = models.ForeignKey(User, default='', related_name='user_followed', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return '{} follows {}'.format(self.user, self.follows)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    about_me = models.CharField(blank=True, max_length=256)
    profile_pic = models.ImageField(blank=True, default='',storage=fs)
    portfolio_site = models.URLField(blank=True, max_length=128)
    follow_list = models.ManyToManyField('self', related_name='follows_list', through=FollowList, symmetrical=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username


class UserContactLink(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='links')
    url = models.URLField(max_length=128)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def get_absolute_url(self):
        # reverse to urls.py
        return reverse('account_app:profile', kwargs={'pk':self.pk})

    def __str__(self):
        return self.url


class MessageWall(models.Model):
    '''
    messages_wall ==== wall_messages
    bad gramatical error 
    '''
    user_writer = models.ForeignKey(get_user_model(), default='', related_name='writes_on_wall',on_delete=models.CASCADE)
    user_wall = models.ForeignKey(User, related_name='messages_wall', on_delete=models.CASCADE)
    text = models.TextField(max_length=256)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} in {} wall, {}'.format(self.user_writer, self.user_wall, self.text)

    class Meta:
        ordering = ["-created_at"]
        unique_together = ["user_wall", "text"]


class CommentMessageWall(models.Model):
    '''
    wall_message is the message on the wall, not the text of the comment for that wall message
    '''
    user_writer = models.ForeignKey(get_user_model(), default='', related_name='writes_on_comment', on_delete=models.CASCADE)
    wall_message = models.ForeignKey(MessageWall, related_name='comments', on_delete=models.CASCADE)
    text = models.TextField(max_length=256)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} said {}'.format(self.user_writer, self.text)



# After user create account, this create a profile for that new user
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)
    instance.profile.save()



0